package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;


public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {
	

	//Iterator Class 
	
	private class DoublyLinkedListIterator<E> implements Iterator<E>{
		
		private Node<E> currentNode;
		
		public DoublyLinkedListIterator() {
			this.currentNode = (Node<E>)header.getNext();
		}
		@Override
		public boolean hasNext() {
			return this.currentNode != header;
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result= null;
				result = this.currentNode.getData();
				this.currentNode = this.currentNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
	}
	
	
	
	 int size=0;
	 Node<E> header =new Node<E>();
	 private Comparator<E> C;

	 
	 public CircularSortedDoublyLinkedList( Comparator<E> comparator) {
		 this.size=0;
		 this.header = new Node<E>();
		 this.header.setNext(header);
		 this.header.setPrev(header);
		 this.C = comparator;
		
	 }
		
	 @Override
		public Iterator<E> iterator() {
			
		 return new DoublyLinkedListIterator<E>();
		}
	 
//Adds elements ascending Alphabetically
		@Override
		public boolean add(E obj) {
			Node<E> temp = new Node<>(obj);
			Node<E> curr = header.getNext();
			if(size==0){
				temp.setNext(header);
				temp.setPrev(header);
				header.setNext(temp);
				header.setPrev(temp);
				size++;
				return true;
				}

//Adds before an element if its more than target
			else{
			while(curr!=header){
			if(C.compare(obj,curr.getData())<=0){
			temp.setNext(curr);
			temp.setPrev(curr.getPrev());
			curr.getPrev().setNext(temp);
			curr.setPrev(temp);
			size++;
			return true;
			}
			curr=curr.getNext();
			}}
			
//Adds at the end of the list meaning its the biggest
			temp.setNext(header);
			temp.setPrev(header.getPrev());
			header.getPrev().setNext(temp);
			header.setPrev(temp);
			size++;
			return true;

			}

		@Override
		public int size() {
	
			return size;
		}

		
		@Override
		public boolean remove(E obj) {

			Node<E> curr = header.getNext();
			while(curr!=header) {
				if((curr.getData().equals(obj))) {
					curr.getPrev().setNext(curr.getNext());
					curr.getNext().setPrev(curr.getPrev());
					curr=null;
					size--;
					return true;
				}
				curr=curr.getNext();	
				}
				
			return false;
		}
		

		@Override
		public boolean remove(int index) {
			if ((index<0) || (index > this.size)) {
				throw new IndexOutOfBoundsException("");
			}
			
			if(index>size) {return false;}
			Node<E> curr = header.getNext();
			int count = 0;
			while(count<index) {
				curr=curr.getNext();
				count++;
			}
			remove(curr.getData());
			return true;
			
		}

		@Override
		public int removeAll(E e) {
			int counter = 0;
			while (this.remove(e)) {
				counter++;
			}
			return counter;
		}
			
		

		@Override
		public E first() {

			return this.header.getNext().getData();
		}

		@Override
		public E last() {

			return (E) this.header.getPrev().getData();
		}

		
	
		@Override
		public E get(int index) {
		
			Node<E> curr = header.getNext();
			int count = 0;
			while(count<index) {
				curr=curr.getNext();
				count++;
			}
			return curr.getData();
			
		}

		//Makes the whole list null
		@Override
		public void clear() {
			while(!(this.isEmpty())) {
				this.remove(0);
				
			}
			
		}
		

		public Node<E> header() {
			return header;
		}

		//Determines if the ID has already been used
		@Override
		public boolean contains(E e) {

			if(size==0) {return false;}
			Node<E> curr = header.getNext();
			while(curr!=header) {
				if(curr.getData().equals(e))
					return true;
				curr=curr.getNext();
			}
			return false;
		}

		@Override
		public boolean isEmpty() {

			return size==0;
		}

		@Override
		public int firstIndex(E e) {

			int index=0;
			Node<E> curr = header.getNext();
			while(curr!=header) {	
				if((curr.getData().equals(e))) {
					return index;
				}
				index++;
				curr=curr.getNext();	
				}
				
			return -1;
		}
			
		
		@Override
		public int lastIndex(E e) {
			int counter = this.counter(e);
			int index = 0;
			Node<E> curr = header.getNext();
			
			while(curr!=header) {	
				if (curr.getData().equals(e)) {
					
					counter --;
					
					if (counter==0) {
						return index;
					}

				}
				
				curr = curr.getNext();
				index++;
				
				
			}

			return -1;
		}

		//Helper method that count the number of Nodes with the element e
		
		public int counter(E e) {
			
			int count =0;
			Node<E> curr = header.getNext();
			
			while(curr!=header) {	
			
				if (curr.getData().equals(e)) {
					count++;
					
				}
				
				curr = curr.getNext();
	
				
			}
			return count;
			
			
		}
		
		
	/// Comparator Class	
		
		private static class DefaultComparator<E> implements Comparator<E> {
			public int compare(E o1, E o2) {
				try { 
					return ((Comparable<E>) o1).compareTo(o2); 
				} catch (ClassCastException e) { 
					throw new IllegalArgumentException("Instantiated data type must be Comparable");
				}
			} 
		}
		
		

		
}
