package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {

	@Override
	public  int compare(Car car1,Car car2 ) {
		String car1C = car1.getCarBrand()+car1.getCarModel()+car1.getCarModelOption();
		String car2C = car2.getCarBrand()+car2.getCarModel()+car2.getCarModelOption();
		return car1C.compareTo(car2C);
	}
	

}


