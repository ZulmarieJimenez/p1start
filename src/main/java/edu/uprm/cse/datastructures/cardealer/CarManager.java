package edu.uprm.cse.datastructures.cardealer;
import javax.ws.rs.Path;
import java.util.ArrayList;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;
import edu.uprm.cse.datastructures.cardealer.util.Node;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;   

       

@Path ("/cars")
public class CarManager {

	  private static CircularSortedDoublyLinkedList<Car> carList = CarList.getInstance();

	  
//Returns all cars in an array
	  @GET
	  @Produces(MediaType.APPLICATION_JSON)
	  public Car[] getCars() {
		  Car[] a = new Car[carList.size()];
		  int index=0;
		  Node<Car> curr = carList.header().getNext();
		  while(curr!= carList.header()) {
			  a[index++]=curr.getData();
			  curr=curr.getNext();
		  }
		  
	    return a;
	  }
	  //Returns all cars matching the given brand
	  @GET
	  @Path ("/brand/{brandname}")
	
	  @Produces(MediaType.APPLICATION_JSON)
	  public   ArrayList<Car> getCars(@PathParam("brandname") String brand) {
		  Node<Car> curr = carList.header().getNext();
		  ArrayList<Car>  a = new ArrayList<Car>();
		  while(curr!= carList.header()) {
			 if(curr.getData().getCarBrand().equals(brand)) {
			  a.add(curr.getData());
			 }
			  curr=curr.getNext();
		  }
		  
	    return a;
	  }  
	  


//Returns the car with matching ID
	  @GET
	  @Path("{id}")
	  @Produces(MediaType.APPLICATION_JSON)
	  public Car getCar(@PathParam("id") long id){
		  Node<Car> curr = carList.header().getNext();
		  while(curr!= carList.header()) {
			  if(curr.getData().getCarId()==id)
				  return curr.getData();
			  curr=curr.getNext();
		  }
		  throw new NotFoundException(("Error car " + id + " not found"));

		  
	  }   

//Adds elements ordered Alphabetically and returns NOT_ACCEPTABLE if ID is in use
	  
	  @POST
		@Path("/add")//funciona ( curl -X POST -i -H "Content-Type: application/json" -d '{"carId" : 3, "carBrand" : "Toyota", "carModel" : "4Runner", "carModelOption" : "XLE", "carPrice" : 42000}' http://localhost:8080/cardealer/cars/add
		@Produces(MediaType.APPLICATION_JSON)
		public Response addCar(Car car){
			
			for (int i = 0; i < carList.size(); i++) {
				if(carList.get(i).getCarId() == car.getCarId()) {
					return Response.status(Response.Status.NOT_ACCEPTABLE).build();
				}
			}
			carList.add(car);
			return Response.status(201).build();
		}
	  

//Updates the car matching the ID
	    @PUT
	    @Path("{id}/update")
	    @Produces(MediaType.APPLICATION_JSON)
	    public Response updateCustomer(Car car, @PathParam("id") long id){
	    	 Node<Car> curr = carList.header().getNext();
			  while(curr!= carList.header()) {
				  if(curr.getData().getCarId()==id) {
					  carList.remove(curr.getData());
				  carList.add(car);
			      return Response.status(Response.Status.OK).build();
				  }
				  curr=curr.getNext();
			  }
	    	
	    	
	    	  return Response.status(Response.Status.NOT_FOUND).build();
	    	
	    	
    }
	    
//Deletes the car matching the ID
	    @DELETE
	    @Produces(MediaType.APPLICATION_JSON)
	    @Path("/{id}/delete")
	    public Response deleteCar(@PathParam("id") long id){
	    	Node<Car> curr = carList.header().getNext();
			  while(curr!= carList.header()) {
				  if(curr.getData().getCarId()==id) {
					  carList.remove(curr.getData());
					  return Response.status(Response.Status.OK).build();
				  }
				  curr=curr.getNext();
	    	  }
	    	  return Response.status(Response.Status.NOT_FOUND).build();      
	     
	      
	    
	    } 
	    
	    
}
