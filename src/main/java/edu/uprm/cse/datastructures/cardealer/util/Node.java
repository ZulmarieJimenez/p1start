package edu.uprm.cse.datastructures.cardealer.util;

public class Node<Car> {
	
//instance variables
	public	Car data;
	public Node next;
	public Node prev;

//constructors
	public Node() {
		next=null;
		prev=null;
		data=null;
	}
	public Node(Car car, Node n, Node p) {
		data=car;
		next=n;
		prev=p;
	}
	public Node(Car car) {
		this.setData(car);
	}
	
//setters
	public void setNext(Node<Car> n) {
		this.next=n;
	}
	public void setPrev(Node<Car> p) {
		this.prev=p;
	}
	public void setData(Car car) {
		this.data=car;
	}
	
//getters
	public Node<Car> getNext() {
		return this.next;
	}
	public Node<Car> getPrev() {
		return this.prev;
	}
	public Car getData() {
		return this.data;
	}
}